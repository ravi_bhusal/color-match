﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Rigidbody2D))]

public class Blocks : MonoBehaviour {

   
    Rigidbody2D rb;
    bool mouseDown = false;

    public AudioSource swooshSound;
    
	void Start ()
    {
        rb = GetComponent<Rigidbody2D>();
       
        
      
	}
	
	
	

    void FixedUpdate()
    {
        Vector2 direction;
        float speed = 14f;

        if (Input.touches[0].phase == TouchPhase.Moved && mouseDown)//Check if Touch has moved.
        {
            direction = Input.touches[0].deltaPosition.normalized;  //Unit Vector of change in position
                                                                    // speed = Input.touches[0].deltaPosition.magnitude / Input.touches[0].deltaTime; //distance traveled divided by time elapsed
           // transform.Translate(direction.x * speed, direction.y * speed, 0);
            rb.velocity=new Vector2(direction.x * speed, direction.y * speed);
            if(PlayerPrefs.GetString("music")==("Music: On"))
            {
                swooshSound.Play();
            }
        }

        Destroy(gameObject, 3.5f);
    }

    void OnMouseDown()
    {
        mouseDown = true;
        
        
    }

   

    

    void OnDestroy()
    {
        if(!mouseDown)
        {
            GameStateManager.EndGame();
        }
    }
}
