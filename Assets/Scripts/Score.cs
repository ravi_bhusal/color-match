﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Advertisements;

public class Score : MonoBehaviour {

    public Text text;
    //Animals animals;
    public int score = 0;

	private static Score instanceRef;


	void Awake()
	{
		if(instanceRef == null)	          
		{
			instanceRef = this;
			DontDestroyOnLoad(gameObject);
		}
		else
		{
			DestroyImmediate(gameObject);
		}
	


	}


    void Start()
    { 

    }

    void Update()
    {
        text.text = "" + score;
    }


    
	void OnLevelWasLoaded(int level)
	{
		if(level==1)
		{
			text.enabled=true;
			text.text = ""+ 0;
			score=0;
		}
		if(level==0)
		{
			text.enabled=false;
		}
		if(level==2)
		{
			text.enabled=false;
		}

	}



   

  
}

