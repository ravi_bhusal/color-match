﻿using UnityEngine;
using System.Collections;

public class Spawners : MonoBehaviour {

    public Transform[] prefab;
    public Transform[] spawnPoints;
    


    Score score;


	[SerializeField]
	float spawnTime=1.5f;
   

	private float curSpawnTime;
	
	void Start()
	{
		curSpawnTime = spawnTime;
		score = FindObjectOfType<Score>();
	}
	
	void Update()
	 {
		curSpawnTime-=1 * Time.deltaTime;
		if(curSpawnTime <= 0)
		{
			Spawn();
            curSpawnTime = spawnTime;

		}

		if (score.score >= 50 && score.score<100) 
		{
			spawnTime=1.25f;
		}
		else if(score.score>=100 && score.score<150)
		{
			spawnTime=1;
		}
        else if(score.score>=150 && score.score<200)
        {
            spawnTime =0.75f;
        }

        else if(score.score>=200)
        {
            spawnTime = 0.6f;
        }

		
	}

    void Spawn()
    {

        Vector3 point = spawnPoints[Random.Range(0, 9)].transform.position;
        Collider2D hitColliders = Physics2D.OverlapCircle(point, 0.33f);
        if (hitColliders == null)
        {
          
            Instantiate(prefab[Random.Range(0, 12)], point, Quaternion.identity);

        }
    }
        

   
}
