﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;


public class GameOver : MonoBehaviour {

	Score score;


	public Text curScore;
	public Text highScore;



	void Start()
	{

		
		
		score=FindObjectOfType<Score>();

		PlayerPrefs.SetInt("High Score", PlayerPrefs.GetInt("High Score"));

	}

	void Update()
	{
		curScore.text=""+ score.score;

		if (score.score > PlayerPrefs.GetInt("High Score"))
		{
			
			PlayerPrefs.SetInt("High Score", score.score);
			PlayerPrefs.Save();			
		}

		highScore.text = "Best: "+PlayerPrefs.GetInt("High Score");
		
	}

	public void OkPress()
	{
		Application.LoadLevel(0);

	}



}
