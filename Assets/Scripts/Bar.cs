﻿using UnityEngine;
using System.Collections;

public class Bar : MonoBehaviour {

    Score score;

    
    void Start ()
    {
        score = FindObjectOfType<Score>();
    }
	
	
	void Update ()
    {
	
	}

    void OnTriggerEnter2D(Collider2D col)
    {
        if(gameObject.tag==col.gameObject.tag)
        {
            Destroy(col.gameObject);
            score.score += 10;
            
        }
        else if(gameObject.tag!=col.gameObject.tag)
        {
            Destroy(col.gameObject);
            GameStateManager.EndGame();
        }
    }


}
