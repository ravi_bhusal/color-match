﻿using UnityEngine;
using System.Collections.Generic;
using Facebook.Unity;

public class GameStateManager : MonoBehaviour
{
    //   Singleton   //
    private static GameStateManager instance;
    public static GameStateManager Instance { get { return current(); } }
    delegate GameStateManager InstanceStep();
    static InstanceStep init = delegate ()
    {
        GameObject container = new GameObject("GameStateManagerManager");
        instance = container.AddComponent<GameStateManager>();
        
        instance.highScore = null;
        current = final;
        return instance;
    };
    static InstanceStep final = delegate () { return instance; };
    static InstanceStep current = init;

    //   Game Config   //
    // Set the ServerURL to a location you are hosting your game assets
  //  public static readonly string ServerURL = "https://friendsmash-unity.herokuapp.com/";
  
    //   Game State   //
    private int score;
    private int? highScore;
    public static bool ScoringLockout, highScorePending;
    public static int Score { get { return Instance.score; } }
    public static int HighScore 
    {
        get { return Instance.highScore.HasValue ? Instance.highScore.Value : 0; }
        set { Instance.highScore = value; }
    }
    
   
   
   
    //   Facebook Data   //
    public static string Username;
    public static Texture UserTexture;
    public static List<object> Friends;
    public static Dictionary<string, Texture> FriendImages = new Dictionary<string, Texture>();
    public static List<object> InvitableFriends = new List<object>();
    // Scores
   
    public static bool ScoresReady;
    private static List<object> scores;
    public static List<object> Scores
    {
        get { return scores; }
        set { scores = value; ScoresReady = true; }
    }

    void Awake()
    {
        // Persist through Scene loading
        DontDestroyOnLoad(this);
      
      
    }

   

   public static void GetScore(int score)
    {
        Instance.score = score;
    }

    public static void EndGame()
    {
      
        Instance.highScore = PlayerPrefs.GetInt("High Score");
        Debug.Log("EndGame Instance.highScore = " + Instance.highScore + "\nInstance.score = " + Instance.score);

        // Log custom App Event for game completion
        //FBAppEvents.GameComplete(Instance.score);

        // Ensure we have read score from FB before we allow overriding the High Score
        if (FB.IsLoggedIn &&
            Instance.highScore.HasValue &&
            Instance.highScore < Instance.score)
        {
            Debug.Log("Player has new high score :" + Instance.score);
            Instance.highScore = Instance.score;

            //Set a flag so MainMenu can handle posting the score once its scene has loaded
            highScorePending = true;
        }

        //Return to main menu
        Application.LoadLevel(2);
    }

  /*  public static void CallUIRedraw()
    {
        GameObject gMenuObj = GameObject.Find("StartMenu");
        if (gMenuObj)
        {
            gMenuObj.GetComponent<StartMenu>().Back();
        }
    }*/
    void Update()
    {
        Screen.sleepTimeout = SleepTimeout.NeverSleep;
    }
}
